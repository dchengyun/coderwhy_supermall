import {request} from './request.js'
export function getHomem () {
	// 
	return request({
		url:'/home/multidata'
	})
}
export function getHomeGoods (type,page) {
	// 
	return request({
		url:'/home/goods',
		params:{
			type,
			page
		}
	})
}